import 'package:resize_splitter/resize_splitter.dart';
import 'dart:math' as math;

class RenderSplitPane extends RenderBox {
  RenderSplitPane({
    Axis orientation = Axis.horizontal,
    double splitRatio = 0.9,
    double splitterThickness = 3.0,
    bool roundToWholePixel = false,
    SplitPaneResizePolicy resizePolicy = SplitPaneResizePolicy.maintainSplitRatio,
    double minWidth = 1,
    double minHeight = 1,
    double maxWidth = 200,
    double maxHeight = 500,
    bool hover = false
  }) {
    this.orientation = orientation;
    this.splitRatio = splitRatio;
    this.splitterThickness = splitterThickness;
    this.roundToWholePixel = roundToWholePixel;
    this.resizePolicy = resizePolicy;
    this.minWidth = minWidth;
    this.hover = hover;
    this.minHeight = minHeight;
    this.maxWidth = maxWidth;
    this.maxHeight = maxHeight;
  }

  double? _minWidth;
  double get minWidth => _minWidth!;
  set minWidth(double value){

    if (_minWidth == value) return;
    if (value >0){
      _minWidth = value;
    }else {
      _minWidth = size.width;
    }
    markNeedsLayout();
  }
  double? _maxWidth;
  double get maxWidth => _maxWidth!;
  set maxWidth(double value){

    if (_maxWidth == value) return;
    if (value >0){
      _maxWidth = value;
    }else {
      _maxWidth = size.width;
    }
    markNeedsLayout();
  }

  double? _minHeight;
  double get minHeight => _minHeight!;
  set minHeight(double value){

    if (_minHeight == value) return;
    if (value > 0){
      _minHeight = value;
    }else {
      _minHeight = size.height;
    }
    markNeedsLayout();
  }
  double? _maxHeight;
  double get maxHeight => _maxHeight!;
  set maxHeight(double value){

    if (_maxHeight == value) return;
    if (value >0){
      _maxHeight = value;
    }else {
      _maxHeight = size.width;
    }
    markNeedsLayout();
  }

  Axis? _orientation;
  Axis get orientation => _orientation!;
  set orientation(Axis value) {
    if (value == _orientation) return;
    _orientation = value;
    markNeedsLayout();
  }

  double? _splitRatio;
  double get splitRatio => _splitRatio!;
  set splitRatio(double value) {
    assert(value >= 0 && value <= 1);
    if (value == _splitRatio) return;
    _splitRatio = value;
    markNeedsLayout();
  }

  double? _splitterThickness;
  double get splitterThickness => _splitterThickness!;
  set splitterThickness(double value) {
    assert(value > 0);
    if (value == _splitterThickness) return;
    _splitterThickness = value;
    markNeedsLayout();
  }

  bool? _roundToWholePixel;
  bool get roundToWholePixel => _roundToWholePixel!;
  set roundToWholePixel(bool value) {
    if (value == _roundToWholePixel) return;
    _roundToWholePixel = value;
    markNeedsLayout();
  }

  bool? _hover;
  bool get hover => _hover!;
  set hover(bool? value) {
    if (value == _hover) return;
    _hover = value;
    markNeedsLayout();
  }

  SplitPaneResizePolicy? _resizePolicy;
  SplitPaneResizePolicy get resizePolicy => _resizePolicy!;
  set resizePolicy(SplitPaneResizePolicy value) {
    if (value == _resizePolicy) return;
    _resizePolicy = value;
  }

  RenderBox? _before;
  RenderBox? get before => _before;
  set before(RenderBox? value) {
    if (value == _before) return;
    if (_before != null) dropChild(_before!);
    _before = value;
    if (_before != null) adoptChild(_before!);
  }

  RenderBox? _after;
  RenderBox? get after => _after;
  set after(RenderBox? value) {
    if (value == _after) return;
    if (_after != null) dropChild(_after!);
    _after = value;
    if (_after != null) adoptChild(_after!);
  }

  RenderBox? _splitter;
  RenderBox? get splitter => _splitter;
  set splitter(RenderBox? value) {
    if (value == _splitter) return;
    if (_splitter != null) dropChild(_splitter!);
    _splitter = value;
    if (_splitter != null) adoptChild(_splitter!);
  }

  double _constrainSplitX(double splitX) {
    if (minWidth >= splitX){

      if (minWidth - splitX.abs() <= 1 || splitX <0){
        return 0 * size.width;
      }
      return minWidth;
    }

    return math.max(math.min(splitX, maxWidth - splitterThickness), 0);
  }

  double _constrainSplitY(double splitY) {
    if (minHeight >= splitY){

      if (minHeight - splitY.abs() <= 1 || splitY <0){
        return 0 * size.height;
      }
      return minHeight;
    }

    return math.max(math.min(splitY, maxHeight - splitterThickness), 0);
  }

  void handleDrag(DragUpdateDetails details) {
    late final double newSplitRatio;
    switch (orientation) {
      case Axis.horizontal:
        final double oldSplitX = _splitRatio! * size.width;
        final double newSplitX = _constrainSplitX(oldSplitX + details.delta.dx);
        newSplitRatio = newSplitX / size.width;
        break;
      case Axis.vertical:
        final double oldSplitY = splitRatio * size.height;
        final double newSplitY = _constrainSplitY(oldSplitY + details.delta.dy);
        newSplitRatio = newSplitY / size.height;
        break;
    }
    splitRatio = newSplitRatio;
  }

  @override
  void attach(PipelineOwner owner) {
    super.attach(owner);
    if (before != null) before!.attach(owner);
    if (after != null) after!.attach(owner);
    if (splitter != null) splitter!.attach(owner);
  }

  @override
  void detach() {
    super.detach();
    if (before != null) before!.detach();
    if (after != null) after!.detach();
    if (splitter != null) splitter!.detach();
  }

  @override
  void visitChildren(RenderObjectVisitor visitor) {
    if (before != null) visitor(before!);
    if (after != null) visitor(after!);
    if (splitter != null) visitor(splitter!);
  }

  @override
  bool hitTestChildren(BoxHitTestResult result, {required Offset position}) {
    //for (RenderBox? child in [ after, before]) {
    for (RenderBox? child in [splitter, after, before]) {
      if (child != null) {
        final BoxParentData parentData = child.parentData as BoxParentData;
        final bool isHit = result.addWithPaintOffset(
          offset: parentData.offset,
          position: position,
          hitTest: (BoxHitTestResult result, Offset transformed) {
            assert(transformed == position - parentData.offset);
            return child.hitTest(result, position: transformed);
          },
        );
        if (isHit) {
          return true;
        }
      }
    }

    return false;
  }

  @override
  double computeMinIntrinsicWidth(double height) => 0;

  @override
  double computeMaxIntrinsicWidth(double height) => computeMinIntrinsicWidth(height);

  @override
  double computeMinIntrinsicHeight(double width) => 0;

  @override
  double computeMaxIntrinsicHeight(double width) => computeMinIntrinsicHeight(width);

  @override
  void performLayout() {
    switch (orientation) {
      case Axis.horizontal:
        assert(constraints.hasTightWidth);
        assert(constraints.hasBoundedHeight);
        final double? previousWidth = hasSize ? size.width : null;
        size = constraints.biggest;

        late double splitX;
        if (previousWidth == null || previousWidth == size.width) {
          splitX = size.width * splitRatio;
        } else {
          switch (resizePolicy) {
            case SplitPaneResizePolicy.maintainSplitRatio:
              splitX = _constrainSplitX(size.width * splitRatio);
              break;
            case SplitPaneResizePolicy.maintainBeforeSplitSize:
              final double oldSplitX = previousWidth * splitRatio;
              splitX = _constrainSplitX(oldSplitX);
              _splitRatio = splitX / size.width;
              break;
            case SplitPaneResizePolicy.maintainAfterSplitSize:
              final double oldSplitX = previousWidth * splitRatio;
              final double deltaWidth = size.width - previousWidth;
              splitX = _constrainSplitX(oldSplitX + deltaWidth);
              _splitRatio = splitX / size.width;
              break;
          }
        }

        if (roundToWholePixel) {
          splitX = splitX.roundToDouble();
        }

        late final BoxConstraints beforeConstraints  = BoxConstraints.tightFor(
          width: splitX,
          height: size.height,
        );
        before!.layout(beforeConstraints);

        double splitterThickness = this.splitterThickness;
        if (roundToWholePixel) {
          splitterThickness = splitterThickness.roundToDouble();
        }

        if(hover) {
          splitter!.layout(BoxConstraints.tightFor(
              width: splitterThickness, height: size.height));

        } else {
          splitter!.layout(BoxConstraints.tightFor(
              width: 1, height: size.height));
        }
        BoxParentData splitterParentData = splitter!
            .parentData as BoxParentData;
        splitterParentData.offset = Offset(splitX, 0);
        final double afterX = splitX + (hover == true ? splitterThickness : 0);
        final BoxConstraints afterConstraints = BoxConstraints.tightFor(
          width: size.width - afterX,
          height: size.height,
        );
        after!.layout(afterConstraints);
        BoxParentData afterParentData = after!.parentData as BoxParentData;
        afterParentData.offset = Offset(afterX, 0);

        break;
      case Axis.vertical:
        assert(constraints.hasTightHeight);
        assert(constraints.hasBoundedWidth);
        final double? previousHeight = hasSize ? size.height : null;
        size = constraints.biggest;

        late double splitY;
        if (previousHeight == null || previousHeight == size.height) {
          splitY = size.height * splitRatio;
        } else {
          switch (resizePolicy) {
            case SplitPaneResizePolicy.maintainSplitRatio:
              splitY = _constrainSplitY(size.height * splitRatio);
              break;
            case SplitPaneResizePolicy.maintainBeforeSplitSize:
              final double oldSplitY = previousHeight * splitRatio;
              splitY = _constrainSplitY(oldSplitY);
              _splitRatio = splitY / size.height;
              break;
            case SplitPaneResizePolicy.maintainAfterSplitSize:
              final double oldSplitY = previousHeight * splitRatio;
              final double deltaHeight = size.height - previousHeight;
              splitY = _constrainSplitY(oldSplitY + deltaHeight);
              _splitRatio = splitY / size.height;
              break;
          }
        }

        if (roundToWholePixel) {
          splitY = splitY.roundToDouble();
        }
        final BoxConstraints beforeConstraints = BoxConstraints.tightFor(
          width: size.width,
          height: splitY,
        );
        before!.layout(beforeConstraints);

        double splitterThickness = this.splitterThickness;
        if (roundToWholePixel) {
          splitterThickness = splitterThickness.roundToDouble();
        }
        if (hover) {
          splitter!.layout(BoxConstraints.tightFor(
              width: size.width, height: splitterThickness));

        }else {
          splitter!.layout(BoxConstraints.tightFor(
              width: size.width, height: 1.0));
        }
        BoxParentData splitterParentData = splitter!
            .parentData as BoxParentData;
        splitterParentData.offset = Offset(0, splitY);
        final double afterY = splitY + splitterThickness;
        final BoxConstraints afterConstraints = BoxConstraints.tightFor(
          width: size.width,
          height: size.height - afterY,
        );
        after!.layout(afterConstraints);
        BoxParentData afterParentData = after!.parentData as BoxParentData;
        afterParentData.offset = Offset(0, afterY);

        break;
    }
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    assert(before != null);
    assert(after != null);
    context.paintChild(before!, offset);
    BoxParentData afterParentData = after!.parentData as BoxParentData;
    context.paintChild(after!, offset + afterParentData.offset);
    BoxParentData splitterParentData = splitter!.parentData as BoxParentData;
    context.paintChild(splitter!, offset + splitterParentData.offset);
  }

  @override
  void redepthChildren() {
    if (before != null) redepthChild(before!);
    if (after != null) redepthChild(after!);
    if (splitter != null) redepthChild(splitter!);
  }

  @override
  List<DiagnosticsNode> debugDescribeChildren() {
    return <DiagnosticsNode>[
      if (before != null) before!.toDiagnosticsNode(name: 'before'),
      if (after != null) after!.toDiagnosticsNode(name: 'after'),
      if (splitter != null) splitter!.toDiagnosticsNode(name: 'splitter'),
    ];
  }
}