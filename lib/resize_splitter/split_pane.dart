import 'package:resize_splitter/resize_splitter.dart';


class SplitPane extends StatelessWidget {
  const SplitPane({
    Key? key,
    this.orientation = Axis.horizontal,
    this.initialSplitRatio = 0.3,
    this.splitterThickness = 3.0,
    this.roundToWholePixel = false,

    this.resizePolicy = SplitPaneResizePolicy.maintainSplitRatio,
    required this.before,
    required this.after,
    this.minWidth,
    this.maxHeight,
    this.maxWidth,
     this.minHeight,
    this.borderColor,
    this.childBorder
  }) : super(key: key);

  final Axis orientation;
  final double initialSplitRatio;
  final double splitterThickness;
  final bool roundToWholePixel;
  final SplitPaneResizePolicy resizePolicy;
  final Widget before;
  final Widget after;
  final double? minHeight;
  final double? minWidth;
  final double? maxHeight;
  final double? maxWidth;
  final Color? borderColor;
  final Widget? childBorder;

  @override
  Widget build(BuildContext context) {
    return RawSplitPane(
      minHeight: minHeight ??200,
      minWidth:minWidth ?? 200,
      maxWidth:maxWidth ?? 500,
      maxHeight:maxHeight?? 500,
      orientation: orientation,
      initialSplitRatio: initialSplitRatio,
      splitterThickness: splitterThickness,
      roundToWholePixel: roundToWholePixel,
      resizePolicy: resizePolicy,
      before: before,
      after: after,
      splitter: Splitter(orientation: orientation, borderColor: borderColor ?? Colors.blueAccent,child: childBorder,),
    );
  }
}

