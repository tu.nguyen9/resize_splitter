
import 'package:resize_splitter/resize_splitter.dart';
class SplitPaneElement extends RenderObjectElement {
  SplitPaneElement(RawSplitPane widget) : super(widget);

  Element? _before;
  Element? _after;
  Element? _splitter;

  @override
  RawSplitPane get widget => super.widget as RawSplitPane;

  @override
  RenderSplitPane get renderObject => super.renderObject as RenderSplitPane;

  @override
  void visitChildren(ElementVisitor visitor) {
    if (_before != null) visitor(_before!);
    if (_after != null) visitor(_after!);
    if (_splitter != null) visitor(_splitter!);
  }

  @override
  void mount(Element? parent, dynamic newSlot) {
    super.mount(parent, newSlot);
    _before = updateChild(_before, widget.before, SplitPaneSlot.before);
    _after = updateChild(_after, widget.after, SplitPaneSlot.after);
    _splitter = updateChild(_splitter, widget.splitter, SplitPaneSlot.splitter);
  }

  @override
  void insertRenderObjectChild(RenderBox child, SplitPaneSlot slot) {
    switch (slot) {
      case SplitPaneSlot.before:
        renderObject.before = child;
        break;
      case SplitPaneSlot.after:
        renderObject.after = child;
        break;
      case SplitPaneSlot.splitter:
        renderObject.splitter = child;
        break;
    }
  }

  @override
  void moveRenderObjectChild(RenderObject child, SplitPaneSlot? oldSlot, SplitPaneSlot? newSlot) {
    assert(false);
  }

  @override
  void update(RenderObjectWidget newWidget) {
    super.update(newWidget);
    _before = updateChild(_before, widget.before, SplitPaneSlot.before);
    _after = updateChild(_after, widget.after, SplitPaneSlot.after);
    _splitter = updateChild(_splitter, widget.splitter, SplitPaneSlot.splitter);
  }

  @override
  void forgetChild(Element child) {
    assert(child == _before || child == _after);
    if (child == _before) {
      _before = null;
    } else if (child == _after) {
      _after = null;
    } else if (child == _splitter) {
      _splitter = null;
    }
    super.forgetChild(child);
  }

  @override
  void removeRenderObjectChild(RenderBox child, SplitPaneSlot? slot) {
    assert(child == renderObject.before || child == renderObject.after || child == renderObject.splitter);
    switch (slot) {
      case SplitPaneSlot.before:
        renderObject.before = null;
        break;
      case SplitPaneSlot.after:
        renderObject.after = null;
        break;
      case SplitPaneSlot.splitter:
        renderObject.splitter = null;
        break;
      case null:
        assert(false);
    }
  }
}