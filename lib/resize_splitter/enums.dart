enum SplitPaneResizePolicy {
  maintainSplitRatio,
  maintainBeforeSplitSize,
  maintainAfterSplitSize,
}

enum SplitPaneSlot{
  before, after, splitter,
}