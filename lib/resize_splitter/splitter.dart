import 'package:resize_splitter/resize_splitter.dart';


class Splitter extends StatelessWidget {
  const Splitter({Key? key, required this.orientation,required this.borderColor, this.child}) : super(key: key);

  final Color borderColor;
  final Axis orientation;
  final Widget? child;

  MouseCursor _cursorForOrientation() {
    switch (orientation) {
      case Axis.horizontal:
        return SystemMouseCursors.resizeLeftRight;
      case Axis.vertical:
        return SystemMouseCursors.resizeUpDown;
    }
  }
  GestureDragUpdateCallback _handleDrag(BuildContext context) {
    return (DragUpdateDetails details) {
      final RenderSplitPane renderObject = context.findAncestorRenderObjectOfType<RenderSplitPane>()!;
      renderObject.handleDrag(details);
    };
  }

  @override
  Widget build(BuildContext context) {
    final GestureDragUpdateCallback handleDrag = _handleDrag(context);
    return StatefulBuilder(
        builder: (context, set) {
          return MouseRegion(
            cursor: _cursorForOrientation(),
            onHover: (event){

              set(()=>context.findAncestorRenderObjectOfType<RenderSplitPane>()!.hover= true);
            },
            onExit: (event)=> set(()=> context.findAncestorRenderObjectOfType<RenderSplitPane>()!.hover= false),
            child: Container(
              color: borderColor,
              width: MediaQuery.of(context).size.height,
              height: MediaQuery.of(context).size.height,
              child: GestureDetector(
                  dragStartBehavior: DragStartBehavior.start,
                  onHorizontalDragUpdate: orientation == Axis.horizontal ? handleDrag : null,
                  onVerticalDragUpdate: orientation == Axis.vertical ? handleDrag : null,
                  child: child ??
                      CustomPaint(
                        painter: SplitterPainter(orientation),
                      )
              ),
            ),
          );
        }
    );
  }
}