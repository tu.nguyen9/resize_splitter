
import 'package:resize_splitter/resize_splitter.dart';
import 'dart:ui'as ui;
import 'dart:math' as math;
class SplitterPainter extends CustomPainter {
  const SplitterPainter(this.orientation);

  final Axis orientation;

  @override
  void paint(Canvas canvas, Size size) {
    late final double imageWidth, imageHeight;
    switch (orientation) {
      case Axis.horizontal:
        imageWidth = size.width - 4;
        imageHeight = math.min(size.height - 4, 8);
        break;
      case Axis.vertical:
        imageWidth =math.min(size.width - 4, 8);
        imageHeight = size.height - 4;
        break;
    }

    if (imageWidth > 0 && imageHeight > 0) {
      double translateX = (size.width - imageWidth) / 2;
      double translateY = (size.height - imageHeight) / 2;
      canvas.translate(translateX, translateY);

      Color dark = Colors.black54;
      Color light = Colors.yellow;

      ui.Paint paint = ui.Paint();
      switch (orientation) {
        case Axis.horizontal:
          paint..style = PaintingStyle.stroke..strokeWidth = 10;
          paint.color = dark;
          canvas.drawLine(const Offset(0, 0.5), Offset(imageWidth, 0.5), paint);
          canvas.drawLine(const Offset(0, 3.5), Offset(imageWidth, 3.5), paint);
          canvas.drawLine(const Offset(0, 6.5), Offset(imageWidth, 6.5), paint);

          paint.color = light;
          canvas.drawLine(const Offset(0, 1.5), Offset(imageWidth, 1.5), paint);
          canvas.drawLine(const Offset(0, 4.5), Offset(imageWidth, 4.5), paint);
          canvas.drawLine(const Offset(0, 7.5), Offset(imageWidth, 7.5), paint);
          break;
        case Axis.vertical:
          paint.style = PaintingStyle.fill;
          final double half = imageHeight / 2;

          paint.color = dark;
          canvas.drawRect(ui.Rect.fromLTWH(0, 0, 2, half), paint);
          canvas.drawRect(ui.Rect.fromLTWH(3, 0, 2, half), paint);
          canvas.drawRect(ui.Rect.fromLTWH(6, 0, 2, half), paint);

          paint.color = light;
          canvas.drawRect(ui.Rect.fromLTWH(0, half, 2, half), paint);
          canvas.drawRect(ui.Rect.fromLTWH(3, half, 2, half), paint);
          canvas.drawRect(ui.Rect.fromLTWH(6, half, 2, half), paint);
          break;
      }
    }
  }

  @override
  bool shouldRepaint(SplitterPainter oldDelegate) {
    return orientation != oldDelegate.orientation;
  }
}