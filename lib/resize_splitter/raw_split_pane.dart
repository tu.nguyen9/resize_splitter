import 'package:resize_splitter/resize_splitter.dart';

class RawSplitPane extends RenderObjectWidget {
  const RawSplitPane({
    Key? key,
    this.orientation = Axis.horizontal,
    required this.initialSplitRatio,
    required this.splitterThickness,
    required this.roundToWholePixel,
    required this.resizePolicy,
    required this.before,
    required this.after,
    required this.splitter,
    required this.minHeight,
    required this.minWidth,
    required this.maxWidth, required this.maxHeight
  }) : super(key: key);

  final Axis orientation;
  final double initialSplitRatio;
  final double splitterThickness;
  final bool roundToWholePixel;
  final SplitPaneResizePolicy resizePolicy;
  final Widget before;
  final Widget after;
  final Widget splitter;
  final double minHeight;
  final double minWidth;
  final double maxHeight;
  final double maxWidth;

  @override
  RenderObjectElement createElement() => SplitPaneElement(this);

  @override
  RenderObject createRenderObject(BuildContext context) {
    return RenderSplitPane(
      orientation: orientation,
      splitRatio: initialSplitRatio,
      splitterThickness: splitterThickness,
      roundToWholePixel: roundToWholePixel,
      resizePolicy: resizePolicy,
      minWidth: minWidth,
      minHeight: minHeight,
      maxHeight: maxHeight,
      maxWidth: maxWidth,
    );
  }

  @override
  void updateRenderObject(BuildContext context, RenderSplitPane renderObject) {
    renderObject
      ..orientation = orientation
      ..splitterThickness = splitterThickness
      ..roundToWholePixel = roundToWholePixel
      ..resizePolicy = resizePolicy
    ..minWidth = minWidth
    ..minHeight = minHeight
    ..maxWidth = maxWidth
    ..maxHeight = maxHeight;
  }
}